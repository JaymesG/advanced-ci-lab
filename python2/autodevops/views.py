from django.views import generic

def server_error(request, template_name='500.html'):
    from django.template import RequestContext
    from django.http import HttpResponseServerError
    t = get_template(template_name)
    return HttpResponseServerError(t.render(RequestContext(request)))

class SinglePageView(generic.TemplateView):
    template_name = 'autodevops/base.html'


class ErrorView(generic.TemplateView):
    template_name = 'autodevops/base.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['division_by_zero'] = 1 / 0
        return context
